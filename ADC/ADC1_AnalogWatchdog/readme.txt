***README***

STARTING POINT
************************************************************************
My starting point was from the ADC1 AnalogWatchdog Example downloaded from 
the IAR Embedded Workbench. The purpose of this program is to continuously 
monitor the ADC voltage controlled by the PC00 potentiometer. The ADC 
voltage is displayed on the LCD screen along with a brief instruction 
to using the program with the evaluation board. When the ADC
voltage goes above a certain voltage, the watchdog feature notifies the
user by turning on LED4 and changing the potentiometer voltage on the LCD
screen from blue to red.

WHY THIS PROJECT?
************************************************************************

