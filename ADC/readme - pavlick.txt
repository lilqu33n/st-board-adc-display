***README***

STARTING POINT
************************************************************************
My starting point was from the ADC1 Analog Watchdog Example downloaded from 
the IAR Embedded Workbench. This example project was found under the 
stm32f0xx_stdperiph_lib. The purpose of this program is to continuously 
monitor the ADC voltage controlled by the PC00 potentiometer. The ADC 
voltage is displayed on the LCD screen along with a brief instruction 
to using the program with the evaluation board. When the ADC
voltage goes above a certain voltage, the watchdog feature notifies the
user by turning on LED4 and changing the potentiometer voltage on the LCD
screen from blue to red.

WHY THIS PROJECT?
************************************************************************
I needed to begin with a project that would allow me to continuously view
the ADC voltage. While the IAR embedded workbench contained multiple 
projects that would serve this function, the ADC1 Analog Watchdog 
Example was the only project preconfigured for the STM32F072 
evaluation board. 

MY PROJECT OVERVIEW
************************************************************************
I wanted a project that would, in addition to displaying the analog voltage,
display the digital value of the voltage. As I did not need the 
functionality of the watchdog, I decided to remove the information 
displayed on the LCD regarding the threshold voltages monitored and
instead used the additional space to display the digitial voltage values.
I decided to use the Tamper button to toggle the display between the 
decimal and hexadecimal values of the voltage.

ADDED FUNCTIONALITIES
************************************************************************
I created a new function that I called Tamper_Toggle to control whether
the decimal or hexadecimal value corresponding to the voltage should be 
displayed. This function used the Tamper button to determine which value
should be displayed. I created a new variable called DIGITAL_STATE, which
was 0 when the decimal value should be displayed and 1 when the hex value
should be displayed. Another variable I created, TAMPER_STATE, indicated
whether the Tamper button was pressed. As TAMPER_STATE was 0 when the button
was pressed and 1 otherwise, I desired a way to determine whether to
toggle DIGITAL_STATE -- that is, to find whether the button had just been 
pressed or if had already been pressed in a previous clock cycle. I created
another variable, PRESSED, which was equal to 1 if the Tamper button had
already been pressed in the previous clock cycle, and 0 if it was not 
pressed. DIGITAL_STATE would flip only if TAMPER_STATE was 0 and PRESSED 
was also 0.

Next, I was able to edit the LCD screen. I removed the color changing 
feature for potentiometer voltages above or below the watchdog thresholds, 
as it was not desired. I also removed text indicating the values of the 
thresholds. Then, I added an additional line for the digital value of the
potentiometer voltage. 

ODDITIES
************************************************************************
The button state is 0 when pressed and 1 when not pressed -- the opposite
of what I would intuitively expect!

The DAC is something that also seems really cool. If I had more time and
an oscilloscope at home I'd love to play around with using the DAC to 
make some projects. The DAC waveform generator is a really cool feature!
My original project idea was to create audio output from the headphone
jack with frequency controlled by the ADC, but I scrapped that after 
realizing it would take a little more time to implement than the amount 
of time I had to spend.

ADDITIONAL NOTES
************************************************************************
I really liked working with this board and the IAR Embedded Systems 
software! The amount of example projects is really useful and the LCD
screen is super helpful for debugging. The preconfigured functions also
have nice descriptions that made them easier to use. I signed up for an evaluation 
license and someone from IAR even called me -- great customer service! :)


